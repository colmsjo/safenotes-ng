Safe Notes
===============


Pre-reqiusites
--------------

Install Xcode if you haven't already (download in App Store).

Install [Android SDK](https://developer.android.com/sdk/index.html)

 * update your path with the adt-XXX/sdk/tools directory (in `~/.bashrc` etc). This is my installation path: `export PATH=$PATH:~/local/adt-bundle-mac-x86_64-20140702/sdk/tools`

 * Run `android` and install the Android API 19 packages

Cordova: `npm install -g cordova`


Installation
------------

Install iOS emulator with: `npm install -g ios-sim`. Running apps on connected
devices require: `npm install -g ios-deploy`

Install javascript libraries: `cd SafeNotes/www; bower install`


```
cordova platform add ios
cordova platform add android
cordova platform add browser

cordova run browser
```

Build everything with: `cordova build`:

 * Run on connected device: `cordova run ios --device`
 * Run in emulator: `cordova emulate ios`

List available iOS platforms: `./platforms/ios/cordova/lib/list-emulator-images`
Emulate a specific device: `cordova emulate ios --target="iPhone-6-Plus"`
